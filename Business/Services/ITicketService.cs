﻿using Entities.Models;
using MongoDB.Bson;

namespace Business.Services
{
    public interface ITicketService
    {
        public void Create(TicketModel ticket);

        public TicketModel? Get(string id);

        public List<TicketModel> List();

        public void Update(TicketModel ticket);

        public void Delete(string id);
    }
}
