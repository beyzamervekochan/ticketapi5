﻿using DataAccess.Repositories;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class TicketService : ITicketService
    {
        private IMongoRepository<TicketModel> mongoRepository;

        public TicketService(IMongoRepository<TicketModel> mongoRepository)
        {
            this.mongoRepository = mongoRepository;
        }

        public void Create(TicketModel ticket)
        {
            mongoRepository.InsertOne(ticket);

        }
        public void Delete(string id)
        {
            mongoRepository.DeleteById(id);
        }

        public TicketModel? Get(string id)
        {
            return mongoRepository.FindById(id);
        }

        public List<TicketModel> List()
        {
            return mongoRepository.AsQueryable().ToList();
        }

        public void Update(TicketModel ticket)
        {
            mongoRepository.ReplaceOne(ticket);
        }

    }
}
