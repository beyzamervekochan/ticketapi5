﻿using Business.Services;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;

namespace TicketApi5.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TicketsController : ControllerBase
    { 
            private readonly ITicketService _ticketsService;

            public TicketsController(ITicketService ticketsService) =>
                _ticketsService = ticketsService;

            [HttpGet]
            public IActionResult Get() =>
                Ok(_ticketsService.List());

            [HttpGet("{id}")]
            public IActionResult Get(string id)
            {
                var ticket = _ticketsService.Get(id);

                if (ticket is null)
                {
                    return NotFound();
                }

                return Ok(ticket);
            }

            [HttpPost]
            public IActionResult Post(TicketModel newTicket)
            {
                _ticketsService.Create(newTicket);

                return Ok(newTicket);
            }

            [HttpPut("{id}")]
            public IActionResult Update(string id, TicketModel updatedTicket)
            {
                var ticket = _ticketsService.Get(id);

                if (ticket is null)
                {
                    return NotFound();
                }

                _ticketsService.Update(updatedTicket);

                return NoContent();
            }

        [HttpDelete("{id}")]
            public IActionResult Delete(string id)
            {
                var ticket = _ticketsService.Get(id);

                if (ticket is null)
                {
                    return NotFound();
                }

                _ticketsService.Delete(id);

                return NoContent();
            }



    }
    
}
