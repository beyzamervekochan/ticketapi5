﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Entities.Models
{
    public class TicketModel :IDocument
    {
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public ObjectId TicketId { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public DateTime OpenDate { get; set; }
        public DateTime FirstReplyDate { get; set; }
        public DateTime SolveDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
